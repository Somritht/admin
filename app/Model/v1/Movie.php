<?php

namespace App\Model\v1;

use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
    protected $table = 'movie';
    protected $guarded = [ "id" ];
}
