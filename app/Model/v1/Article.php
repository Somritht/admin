<?php

namespace App\Model\v1;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Article extends Model
{
    protected $table='article';
    protected $guarded = [];
    use SoftDeletes;
}
