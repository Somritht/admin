<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\v1\ArticleRequest;
use App\Model\v1\Article;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ArticleController extends Controller
{
    private $rules = [
        'title' => 'required',
        'image' => 'required',
        'body' => 'required',
        'category_id' => 'required'

    ];
    public function index()
    {
        $articles = Article::paginate(10);
        return response()->json($articles, 200);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), $this->rules);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()]);
        }
        $allRequest = $request->all();
        $allRequest['view'] = 0;
        Article::create($allRequest);
        return response()->json(['message' => 'created successfully']);
    }
    public function show($id)
    {
        $articles = Article::findOrFail($id);
        return response()->json($articles, 200);
    }
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), $this->rules);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()]);
        }
        $allRequest = $request->all();
        $article = Article::findOrFail($id);
        $article->update($allRequest);
        return response()->json(['message' => 'updated successfully']);
    }
    public function destroy($id)
    {
        Article::findOrFail($id)->delete();
        return response()->json(['message' => 'deleted successfully']);
    }
}
