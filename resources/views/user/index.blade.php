@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col">
            <div class="card">
                <!-- Card header -->
                <div class="card-header border-0">
                    <h3 class="mb-0">Users <a href="{{ route('user.create') }}" class="btn btn-sm btn-default ml-3">Add
                            New</a></h3>
                </div>
                <!-- Light table -->
                <div class="table-responsive">
                    <table class="table align-items-center table-flush">
                        <thead class="thead-light">
                            <tr>
                                <th scope="col" class="sort" data-sort="name">No</th>
                                <th scope="col" class="sort" data-sort="budget">Name</th>
                                <th scope="col" class="sort" data-sort="status">Email</th>
                                <th scope="col">Actions</th>
                            </tr>
                        </thead>
                        <tbody class="list">
                            @foreach ($users as $item)
                                <tr>
                                    <td class="budget">
                                        {{ $loop->iteration }}
                                    </td>
                                    <th scope="row">
                                        <div class="media align-items-center">
                                            <a href="#" class="avatar rounded-circle mr-3">
                                                <img alt="Image placeholder" src="/user.png">
                                            </a>
                                            <div class="media-body">
                                                <span class="name mb-0 text-sm">{{ $item->name }}</span>
                                            </div>
                                        </div>
                                    </th>
                                    <td>
                                        {{ $item->email }}
                                    </td>
                                    <td>
                                        @if ($item->id != 1)
                                        <form action="{{ route('user.destroy', $item->id) }}" method="POST">
                                           @method('DELETE')
                                           @csrf
                                            <button type="submit"><i class="fa fa-trash" style="font-size: 20px;"></i></button>
                                        
                                    
                                        </form>
                                          
                                        @endif
                                        <a href="{{ route('user.edit', $item->id) }}"><i class="fa fa-pencil"
                                                style="font-size: 20px;"></i></a>
                                    </td>
                                </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
                <div class="card-footer py-4">
                    {{ $users->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection
