@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col">
            <div class="card">
                <!-- Card header -->
                <div class="card-header border-0">
                    <h3 class="mb-0">Movies <a href="{{ route('movie.create') }}" class="btn btn-sm btn-default ml-3">Add
                            New</a></h3>
                </div>
                <!-- Light table -->
                <div class="table-responsive">
                    <table class="table align-items-center table-flush">
                        <thead class="thead-light">
                            <tr>
                                <th scope="col" class="sort" data-sort="name">No</th>
                                <th scope="col" class="sort" data-sort="budget">Name</th>
                                <th scope="col" class="sort" data-sort="status">URL</th>
                                <th scope="col">Actions</th>
                            </tr>
                        </thead>
                        <tbody class="list">
                            @foreach ($movies as $item)
                                <tr>
                                    <td class="budget">
                                        {{ $loop->iteration }}
                                    </td>
                                    <th scope="row">
                                        <div class="media align-items-center">
                                            <a href="#" class="avatar rounded-circle mr-3">
                                                <img alt="Image placeholder" src="{{ $item->image }}">
                                            </a>
                                            <div class="media-body">
                                                <span class="name mb-0 text-sm">{{ $item->name }}</span>
                                            </div>
                                        </div>
                                    </th>
                                    <td>
                                        {{ $item->url }}
                                    </td>
                                    <td>

                                        <form action="{{ route('movie.destroy', $item->id) }}" method="POST">
                                            @method('DELETE')
                                            @csrf
                                            <button type="submit"><i class="fa fa-trash"
                                                    style="font-size: 20px;"></i></button>


                                        </form>
                                    </td>
                                </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
                <div class="card-footer py-4">
                    {{ $movies->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection
