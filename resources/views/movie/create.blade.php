@extends('layouts.app')
@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('New Movie') }}</div>
                <div class="card-body">
                    <form method="POST" action="{{ route('movie.store') }}">
                        @csrf
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" id="name" name="name">
                        </div>
                        <div class="form-group">
                            <label for="email">Url</label>
                            <input type="text" class="form-control" id="email" name="url">
                        </div>
                        <div class="form-group col-md-4 text-center">
                            <label for="profile_image d-block">Image</label>
                            <div class="custom-file mt-3 d-block m-auto" style="width: 150px;height:150px;">
                                <div class="mb-4" style="width: 100%;height:100%;" class="position-relative">
                                    <img src="{{ $product->image ?? 'https://icons-for-free.com/iconfiles/png/512/add+photo+instagram+upload+icon-1320184027593509107.png' }}"
                                        id="thumbnail"
                                        class="img-responsive  img-thumbnail rounded-circle position-absolute" alt="image"
                                        style="width:150px;height:150px;">
                                    <input type="file" style="width: 100%;height:100%;top:0;left:0;"
                                        class=" custom-file-input" name="image"
                                        onchange="console.log(this.value);document.getElementById('thumbnail').src=window.URL.createObjectURL(this.files[0]);">
                                    <div class=" position-absolute img-thumbnail " style="bottom:0;right:0;">
                                        <i class="fa fa-image"></i> Upload
                                    </div>
                                </div>
                            </div>
                            <label for="profile_image d-block">Or using CDN :</label>


                            <input type="text" class="form-control" id="thumbnail_cdn" placeholder="https://example.com/..."
                                value="{{ $product->image ?? '' }}" name="image" aria-describedby="basic-addon3">
                            <button class="btn btn-outline-secondary btn-sm mt-3 text-center" type="button"
                                onclick="document.getElementById('thumbnail').src=document.getElementById('thumbnail_cdn').value;">Check</button>

                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
                <div class="card-footer">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
