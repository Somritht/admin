<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::get('/login', function () {
    return response()->json([
        'status' => 'unauthorized',
        'message' => 'You not have permission to access this!'
    ], 201);
})->name('login');

Route::group(['prefix' => 'v1'], function () {
    Route::get('/article', 'v1\ArticleController@index');
    Route::resource('movie', 'v1\MovieController', ['only' => [ 'index', 'show' ]]);
    // Route::group(['prefix' => 'admin'], function () {

    //     Route::post('/login', 'UserController@login');
    //     Route::post('/signup', 'UserController@signup');
    //     Route::post('/register', 'UserController@register');

    //     Route::group(['middleware' => 'auth:api'], function () {
    //         Route::resource('article', 'v1\ArticleController', ['only' => [
    //             'index', 'show', 'store', 'update', 'destroy'
    //         ]]);
    //         Route::get('/logout', 'UserController@logout');
    //     });
    // });
});
